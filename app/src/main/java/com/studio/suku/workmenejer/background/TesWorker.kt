package com.studio.suku.workmenejer.background

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters

class TesWorker (ctx: Context, params: WorkerParameters) : Worker(ctx, params) {
    override fun doWork(): Result {
        generateNotification("Ini Dibuat Dari Background", applicationContext)
        return Result.success()
    }

}
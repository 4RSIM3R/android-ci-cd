package com.studio.suku.workmenejer.background

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.studio.suku.workmenejer.constant.KEY_IMAGE_URI
import timber.log.Timber
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class SaveImageWorker (ctx : Context, params : WorkerParameters) : Worker(ctx, params) {

    private val Title = "Gambar Blereng"
    private val dateFormatter = SimpleDateFormat(
        "yyyy.MM.dd 'at' HH:mm:ss z",
        Locale.getDefault()
    )

    override fun doWork(): Result {
        generateNotification("Saving Image", applicationContext)

        sleep()

        val resolver = applicationContext.contentResolver

        return try {

            val resourceUri = inputData.getString(KEY_IMAGE_URI)
            val bitmap = BitmapFactory.decodeStream(
                resolver.openInputStream(Uri.parse(resourceUri)))
            val imageUrl = MediaStore.Images.Media.insertImage(
                resolver, bitmap, Title, dateFormatter.format(Date()))
            if (!imageUrl.isNullOrEmpty()) {
                val output = workDataOf(KEY_IMAGE_URI to imageUrl)
                Result.success()
            } else {
                Timber.e("Writing to MediaStore failed")
                Result.failure()
            }

        }catch (exception : Exception){
            Timber.e(exception)
            Result.failure()
        }
    }

}